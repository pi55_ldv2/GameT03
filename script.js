var matrixBlock = document.getElementById("matrix");
var maxValue;
var minValue;
var genBut = document.getElementById("genBut");
var w,h;
var table;
var i,j,k;

var arr = new Array();
function matrixGen() {
    w = document.getElementById("width").value;
    h = document.getElementById("height").value;
    maxValue = document.getElementById("maxV").value;
    minValue = document.getElementById("minV").value;
    for(let i = 0; i < w; i++){
        arr[i] = new Array();
        for(let j = 0; j < h; j++){
            arr[i][j] = new Array();
            for(let k = 0; k < 2; k++) {
                arr[i][j][k] = randomInteger(minValue, maxValue);
            }
        }
    }

    console.table(arr);
}
function genTable() {
    matrixGen();
    if(matrixBlock.childNodes.length > 0){
        matrixBlock.removeChild(matrixBlock.firstChild);
    }

    table = document.createElement('table');
    var html = '<tbody>';
    for (let i = -1; i <= h; i++) {
        html += '<tr>';
            for (let j = -1; j <= w; j++) {
                html += '<td></td>';
            }
        table.innerHTML = html + '</tr></tbody>';
    }
    matrixBlock.appendChild(table);
    for( i = 1; i <= h; i++) {
        for (j = 1; j <= w; j++) {
            for (k = 0; k < 2; k++) {
                table.rows[i].cells[j].innerHTML += arr[j - 1][i - 1][k] + '; ';
            }
        }
    }

    for(let i = 1; i <= h; i++) {
        table.rows[i].cells[0].innerHTML = "B" + i;
        table.rows[i].cells[++w].innerHTML='-';
        table.rows[i].cells[w].setAttribute('class', 'remove');
        w--;

    }
    for(let i = 1; i <= w; i++) {
        table.rows[0].cells[i].innerHTML = "A" + i;
        table.rows[++h].cells[i].innerHTML='-';
        table.rows[h].cells[i].setAttribute('class', 'remove');
        h--;
    }


}
function deleteStratagyA(ih) {
        for (i = 0; i <= h + 1; i++)
            table.firstChild.rows[i].removeChild(table.rows[i].cells[ih]);

}
function deleteStratagyB(obj) {
        table.firstChild.removeChild(table.rows[obj]);
}


matrixBlock.addEventListener('click', function (event) {
    if(event.target.cellIndex === w+1 && event.target.innerHTML!=='') {
        deleteStratagyB(event.target.parentNode.rowIndex);
        h--;
    }

    if(event.target.parentNode.rowIndex === h+1 && event.target.cellIndex !== 0 && event.target.cellIndex !== w+1 ) {
        deleteStratagyA(event.target.cellIndex);
        w--;
    }

    if(w<=1){
        deleteStratagyB(h+1);
    }
    if(h<=1){
        deleteStratagyA(w+1);
    }

});

genBut.onclick = genTable;

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

window.onload = function () {
    matrixGen();
}